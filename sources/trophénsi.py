﻿from tkinter import *
from time import *


#######################
#La matrice initiale : 
#      0 est le vide
#      1 est le bleu
#      2 est le jaune
#      5 est le orange
#      4 est le blanc
#      3 est le rouge
#      1 est le vert
#######################


cube3=[[0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0]]



fenetre=Tk()
fenetre.title("Solver Rubik's Cube")

largeur=len(cube3[0])+10
hauteur=len(cube3)+5
taille=40                                          #ne pas changer sinon ça bug
canvas = Canvas(fenetre, width=taille*largeur, height=taille*hauteur, background='white')





def afficher(Grille):
    global fenetre
    global canvas
    global largeur
    global taille
    global hauteur
    texte = '''
    Garder la face blanche en face de soi, pour une facilité de compréhension.
    Lors de la résolution, 9 cases de chaques couleurs sont nécessaire.
    Lors de la résolution, on ne peut pas avoir 2 cases de même couleur sur un angle ou une arrête
    C'est la même chose pour les couleurs opposées (blanc-jaune, bleu-vert, orange-rouge)  
    Lors de la résolution, 2 angles ne peuvent pas être les mêmes.
    Lors de la résolution, 2 arrêtes ne peuvent pas être les mêmes.
        '''
 
    Label(fenetre, text=texte, width='70').place(x=200,y=430)

    num_line=0
    num_case=0
    for line in Grille:
        num_line=num_line+1
        for case in line:
            num_case=num_case+1
            if case==0:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='white', outline="white")
            if case==1:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='blue1', activefill="blue3",outline="blue4")
            if case==6:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='green1',activefill="green3",outline="green4")
            if case==3:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='red1',activefill="red3",outline="red4")
            if case==4:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='white', activefill="grey80",outline="grey80")
            if case==5:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='orange1',activefill="orange3",outline="orange4")
            if case==2:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='yellow1', activefill="yellow3",outline="yellow4")
            if case==7:
                canvas.create_rectangle((num_case-1)*taille, (num_line-1)*taille, num_case*taille, num_line*taille, fill='pink1', activefill="pink3",outline="pink4")
        num_case=0
    canvas.pack()
    fenetre.bind("<Button>",clique)
    fenetre.mainloop()



def reinit():
    """Cette fonction est activée lorsque le bouton
    Réinitialiser est actionné, elle remet la grille,
    ou le rubik's cube dans son état par défaut"""
    global cube3
    cube3=[[0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [2, 2, 2, 5, 5, 5, 4, 4, 4, 3, 3, 3, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 6, 6, 6, 0, 0, 0, 0]]
    afficher(cube3)

couleur=4
def bleu():
    global couleur
    couleur=1
    return couleur
def jaune():
    global couleur
    couleur=2
    return couleur
def rouge():
    global couleur
    couleur=3
    return couleur
def blanc():
    global couleur
    couleur=4
    return couleur
def orange():
    global couleur
    couleur=5
    return couleur
def vert():
    global couleur
    couleur=6
    return couleur

def resoudre():
    global cube3
    num_line=1
    num_case=1
    print(cube3)
    """Contrainte d'angles: 2 angles de peuvent pas être pareils, pour vérifier
    cela, je multiplie les 3 faces d'un angle entre elles, et compare avec le
    produit des 3 faces d'un autre angle. Si le resultat est le même, les angles
    sont égaux et la résolution du rubik's cube impossible"""
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[2][8]*cube3[3][8]*cube3[3][9]:
        text=Label(fenetre,text="deux angles ",fg='red',bg="grey90")
        text.place(x=100,y=10)
        text.after(3000,text.destroy)
        
        
        
        
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[5][5]*cube3[5][6]*cube3[6][6]:
        print("impossible, deux angles sont pareils")
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[5][8]*cube3[5][9]*cube3[6][8]:
        print("impossible, deux angles sont pareils")
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[0][8]*cube3[3][0]*cube3[3][11]:
        print("impossible, deux angles sont pareils")
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")
    if cube3[2][6]*cube3[3][6]*cube3[3][5]==cube3[8][8]*cube3[5][0]*cube3[5][11]:
        print("impossible, deux angles sont pareils")



    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[0][8]*cube3[3][0]*cube3[3][11]:
        print("impossible, deux angles sont pareils")
    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")
    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[8][8]*cube3[5][0]*cube3[5][11]:
        print("impossible, deux angles sont pareils")
    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[5][5]*cube3[5][6]*cube3[6][6]:
        print("impossible, deux angles sont pareils")
    if cube3[2][8]*cube3[3][8]*cube3[3][9]==cube3[5][8]*cube3[5][9]*cube3[6][8]:
        print("impossible, deux angles sont pareils")



    if cube3[5][5]*cube3[5][6]*cube3[6][6]==cube3[5][8]*cube3[5][9]*cube3[6][8]:
        print("impossible, deux angles sont pareils")
    if cube3[5][5]*cube3[5][6]*cube3[6][6]==cube3[0][8]*cube3[3][0]*cube3[3][11]:
        print("impossible, deux angles sont pareils")
    if cube3[5][5]*cube3[5][6]*cube3[6][6]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[5][5]*cube3[5][6]*cube3[6][6]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")
    if cube3[5][5]*cube3[5][6]*cube3[6][6]==cube3[8][8]*cube3[5][0]*cube3[5][11]:
        print("impossible, deux angles sont pareils")


    if cube3[8][5]*cube3[8][6]*cube3[5][9]==cube3[0][8]*cube3[3][0]*cube3[3][11]:
        print("impossible, deux angles sont pareils")
    if cube3[8][5]*cube3[8][6]*cube3[5][9]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[8][5]*cube3[8][6]*cube3[5][9]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")
    if cube3[8][5]*cube3[8][6]*cube3[5][9]==cube3[8][8]*cube3[5][0]*cube3[5][11]:
        print("impossible, deux angles sont pareils")

    if cube3[8][8]*cube3[5][0]*cube3[5][11]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")
    if cube3[8][8]*cube3[5][0]*cube3[5][11]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[8][8]*cube3[5][0]*cube3[5][11]==cube3[0][8]*cube3[3][0]*cube3[3][11]:
        print("impossible, deux angles sont pareils")

    if cube3[0][8]*cube3[3][0]*cube3[3][11]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")
    if cube3[0][8]*cube3[3][0]*cube3[3][11]==cube3[8][6]*cube3[5][3]*cube3[5][2]:
        print("impossible, deux angles sont pareils")

    if cube3[8][6]*cube3[5][3]*cube3[5][2]==cube3[0][6]*cube3[3][3]*cube3[3][2]:
        print("impossible, deux angles sont pareils")

    """
    Contraite d'arrête: deux arrêtes ne peuvent pas être pareilles:
    pour cela, j'ajoute 2 aux 2 faces d'une arrête , et ensuite je multiplie
    les 2 faces. si 2 résultats sont les mêmes, deux arr^tes sont pareils
    """
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[1][8]+3)*(cube3[3][10]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[5][10]+3)*(cube3[7][8]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[5][4]+3)*(cube3[7][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[4][8]+3)*(cube3[4][9]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[4][5]+3)*(cube3[4][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][6]+3)*(cube3[3][4]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")

    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[5][10]+3)*(cube3[7][8]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[5][4]+3)*(cube3[7][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[4][8]+3)*(cube3[4][9]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[4][5]+3)*(cube3[4][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[1][8]+3)*(cube3[3][10]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")

    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[5][4]+3)*(cube3[7][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[4][8]+3)*(cube3[4][9]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[4][5]+3)*(cube3[4][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][10]+3)*(cube3[7][8]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[4][8]+3)*(cube3[4][9]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[4][5]+3)*(cube3[4][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][4]+3)*(cube3[7][6]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[4][5]+3)*(cube3[4][6]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][8]+3)*(cube3[4][9]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[5][7]+3)*(cube3[6][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][5]+3)*(cube3[4][6]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[5][7]+3)*(cube3[6][7]+3)==(cube3[2][7]+3)*(cube3[3][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][7]+3)*(cube3[6][7]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][7]+3)*(cube3[6][7]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][7]+3)*(cube3[6][7]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[5][7]+3)*(cube3[6][7]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[2][7]+3)*(cube3[3][7]+3)==(cube3[4][3]+3)*(cube3[4][2]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[2][7]+3)*(cube3[3][7]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[2][7]+3)*(cube3[3][7]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[2][7]+3)*(cube3[3][7]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    if (cube3[4][3]+3)*(cube3[4][2]+3)==(cube3[3][1]+3)*(cube3[1][7]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][3]+3)*(cube3[4][2]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[4][3]+3)*(cube3[4][2]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")

    if (cube3[3][1]+3)*(cube3[1][7]+3)==(cube3[8][7]+3)*(cube3[5][1]+3):
        print("impossible, deux arrêtes sont pareilles")
    if (cube3[3][1]+3)*(cube3[1][7]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")

    if (cube3[8][7]+3)*(cube3[5][1]+3)==(cube3[4][11]+3)*(cube3[4][0]+3):
        print("impossible, deux arrêtes sont pareilles")


    """Contrainte de cases: lors de la résolution, le nombre de cases vertes ne
    doivent pas dépasser 9, et pareil pour toutes les couleurs, sinon la
    résolution est impossible
    """


    numbleu=0
    numorange=0
    numjaune=0
    numvert=0
    numrouge=0
    numblanc=0
    for line in cube3:
        num_line+=1
        for case in line:
            num_case+=1
            if case==1:
                numbleu+=1
            if case==2:
                numjaune+=1
            if case==5:
                numorange+=1
            if case==4:
                numblanc+=1
            if case==3:
                numrouge+=1
            if case==6:
                numvert+=1
    if numbleu!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en bleu ",fg='red',bg="white")
        text.place(x=780,y=10)
        text.after(5000,text.destroy)
    if numorange!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en orange",fg='red',bg="white")
        text.place(x=767,y=30)
        text.after(5000,text.destroy)
    if numvert!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en vert",fg='red',bg="white")
        text.place(x=785,y=50)
        text.after(5000,text.destroy)
    if numjaune!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en jaune",fg='red',bg="white")
        text.place(x=775,y=70)
        text.after(5000,text.destroy)
    if numblanc!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en blanc",fg='red',bg="white")
        text.place(x=775,y=90)
        text.after(5000,text.destroy)
    if numrouge!=9 :
        text=Label(fenetre,text="Il n'y a pas 9 cases en rouge",fg='red',bg="white")
        text.place(x=775,y=110)
        text.after(5000,text.destroy)

    """Contrainte de couleur: 2 cases de même couleur ou de couleur opposées
    (jaune et blanc, vert et bleu, rouge et orange) ne peuvent être
    sur la même arrête"""
    if cube3[4][6]==cube3[4][5]:
        print("non")
    if cube3[5][6]==cube3[5][5]:
        print("non")
    if cube3[3][6]==cube3[3][5]:
        print("non")

    if cube3[4][8]==cube3[4][9]:
        print("non")
    if cube3[5][8]==cube3[5][9]:
        print("non")
    if cube3[3][8]==cube3[3][9]:
        print("non")

    if cube3[3][8]==cube3[2][8]:
        print("non")
    if cube3[3][7]==cube3[2][7]:
        print("non")
    if cube3[3][6]==cube3[2][6]:
        print("non")

    if cube3[6][8]==cube3[5][8]:
        print("non")
    if cube3[6][7]==cube3[5][7]:
        print("non")
    if cube3[6][6]==cube3[5][6]:
        print("non")

    if cube3[2][8]==cube3[3][9]:
        print("non")
    if cube3[1][8]==cube3[3][10]:
        print("non")
    if cube3[0][8]==cube3[3][11]:
        print("non")

    if cube3[3][3]==cube3[6][0]:
        print("non")
    if cube3[3][4]==cube3[6][1]:
        print("non")
    if cube3[3][5]==cube3[6][2]:
        print("non")

    if cube3[0][6]==cube3[3][2]:
        print("non")
    if cube3[0][7]==cube3[3][1]:
        print("non")
    if cube3[0][8]==cube3[3][0]:
        print("non")

    if cube3[3][0]==cube3[3][11]:
        print("non")
    if cube3[4][0]==cube3[4][11]:
        print("non")
    if cube3[5][0]==cube3[5][11]:
        print("non")

    if cube3[5][9]==cube3[6][8]:
        print("non")
    if cube3[5][10]==cube3[7][8]:
        print("non")
    if cube3[5][11]==cube3[8][8]:
        print("non")

    if cube3[5][3]==cube3[6][6]:
        print("non")
    if cube3[5][4]==cube3[7][6]:
        print("non")
    if cube3[5][5]==cube3[8][6]:
        print("non")

    if cube3[5][0]==cube3[8][8]:
        print("non")
    if cube3[5][1]==cube3[8][7]:
        print("non")
    if cube3[5][2]==cube3[8][6]:
        print("non")

    """Contrainte de couleur, mais faces opposées"""
    if (cube3[5][2]==1 and cube3[8][6]==6) or (cube3[5][2]==6 and cube3[8][6]==1):
        print("impossible")
    if (cube3[5][2]==3 and cube3[8][6]==5) or (cube3[5][2]==5 and cube3[8][6]==3):
        print("impossible")
    if (cube3[5][2]==2 and cube3[8][6]==4) or (cube3[5][2]==4 and cube3[8][6]==2):
        print("impossible")

    if (cube3[5][3]==1 and cube3[8][6]==6) or (cube3[5][3]==6 and cube3[8][6]==1):
        print("impossible")
    if (cube3[5][3]==3 and cube3[8][6]==5) or (cube3[5][3]==5 and cube3[8][6]==3):
        print("impossible")
    if (cube3[5][3]==2 and cube3[8][6]==4) or (cube3[5][3]==4 and cube3[8][6]==2):
        print("impossible")

    if (cube3[5][4]==1 and cube3[7][6]==6) or (cube3[5][4]==6 and cube3[7][6]==1):
        print("impossible")
    if (cube3[5][4]==3 and cube3[7][6]==5) or (cube3[5][4]==5 and cube3[7][6]==3):
        print("impossible")
    if (cube3[5][4]==2 and cube3[7][6]==4) or (cube3[5][4]==4 and cube3[7][6]==2):
        print("impossible")

    if (cube3[5][5]==1 and cube3[6][6]==6) or (cube3[5][5]==6 and cube3[6][6]==1):
        print("impossible")
    if (cube3[5][5]==3 and cube3[6][6]==5) or (cube3[5][5]==5 and cube3[6][6]==3):
        print("impossible")
    if (cube3[5][5]==2 and cube3[6][6]==4) or (cube3[5][5]==4 and cube3[6][6]==2):
        print("impossible")

    if (cube3[5][6]==1 and cube3[6][6]==6) or (cube3[5][6]==6 and cube3[6][6]==1):
        print("impossible")
    if (cube3[5][6]==3 and cube3[6][6]==5) or (cube3[5][6]==5 and cube3[6][6]==3):
        print("impossible")
    if (cube3[5][6]==2 and cube3[6][6]==4) or (cube3[5][6]==4 and cube3[6][6]==2):
        print("impossible")

    if (cube3[5][2]==1 and cube3[5][3]==6) or (cube3[5][2]==6 and cube3[5][3]==1):
        print("impossible")
    if (cube3[5][2]==3 and cube3[5][3]==5) or (cube3[5][2]==5 and cube3[5][3]==3):
        print("impossible")
    if (cube3[5][2]==2 and cube3[5][3]==4) or (cube3[5][2]==4 and cube3[5][3]==2):
        print("impossible")

    if (cube3[5][5]==1 and cube3[5][6]==6) or (cube3[5][5]==6 and cube3[5][6]==1):
        print("impossible")
    if (cube3[5][5]==3 and cube3[5][6]==5) or (cube3[5][5]==5 and cube3[5][6]==3):
        print("impossible")
    if (cube3[5][5]==2 and cube3[5][6]==4) or (cube3[5][5]==4 and cube3[5][6]==2):
        print("impossible")

    if (cube3[5][0]==1 and cube3[8][8]==6) or (cube3[5][0]==6 and cube3[8][8]==1):
        print("impossible")
    if (cube3[5][0]==3 and cube3[8][8]==5) or (cube3[5][0]==5 and cube3[8][8]==3):
        print("impossible")
    if (cube3[5][0]==2 and cube3[8][8]==4) or (cube3[5][0]==4 and cube3[8][8]==2):
        print("impossible")

    if (cube3[5][10]==1 and cube3[7][8]==6) or (cube3[5][10]==6 and cube3[7][8]==1):
        print("impossible")
    if (cube3[5][10]==3 and cube3[7][8]==5) or (cube3[5][10]==5 and cube3[7][8]==3):
        print("impossible")
    if (cube3[5][10]==2 and cube3[7][8]==4) or (cube3[5][10]==4 and cube3[7][8]==2):
        print("impossible")

    if (cube3[5][9]==1 and cube3[6][8]==6) or (cube3[5][9]==6 and cube3[6][8]==1):
        print("impossible")
    if (cube3[5][9]==3 and cube3[6][8]==5) or (cube3[5][9]==5 and cube3[6][8]==3):
        print("impossible")
    if (cube3[5][9]==2 and cube3[6][8]==4) or (cube3[5][9]==4 and cube3[6][8]==2):
        print("impossible")

    if (cube3[5][8]==1 and cube3[6][8]==6) or (cube3[5][8]==6 and cube3[6][8]==1):
        print("impossible")
    if (cube3[5][8]==3 and cube3[6][8]==5) or (cube3[5][8]==5 and cube3[6][8]==3):
        print("impossible")
    if (cube3[5][8]==2 and cube3[6][8]==4) or (cube3[5][8]==4 and cube3[6][8]==2):
        print("impossible")

    if (cube3[5][8]==1 and cube3[5][9]==6) or (cube3[5][8]==6 and cube3[5][9]==1):
        print("impossible")
    if (cube3[5][8]==3 and cube3[5][9]==5) or (cube3[5][8]==5 and cube3[5][9]==3):
        print("impossible")
    if (cube3[5][8]==2 and cube3[5][9]==4) or (cube3[5][8]==4 and cube3[5][9]==2):
        print("impossible")

    if (cube3[5][0]==1 and cube3[5][11]==6) or (cube3[5][0]==6 and cube3[5][11]==1):
        print("impossible")
    if (cube3[5][0]==3 and cube3[5][11]==5) or (cube3[5][0]==5 and cube3[5][11]==3):
        print("impossible")
    if (cube3[5][0]==2 and cube3[5][11]==4) or (cube3[5][0]==4 and cube3[5][11]==2):
        print("impossible")

    if (cube3[0][6]==1 and cube3[3][2]==6) or (cube3[0][6]==6 and cube3[3][2]==1):
        print("impossible")
    if (cube3[0][6]==3 and cube3[3][2]==5) or (cube3[0][6]==5 and cube3[3][2]==3):
        print("impossible")
    if (cube3[0][6]==2 and cube3[3][2]==4) or (cube3[0][6]==4 and cube3[3][2]==2):
        print("impossible")

    if (cube3[0][6]==1 and cube3[3][3]==6) or (cube3[0][6]==6 and cube3[3][3]==1):
        print("impossible")
    if (cube3[0][6]==3 and cube3[3][3]==5) or (cube3[0][6]==5 and cube3[3][3]==3):
        print("impossible")
    if (cube3[0][6]==2 and cube3[3][3]==4) or (cube3[0][6]==4 and cube3[3][3]==2):
        print("impossible")

    if (cube3[1][6]==1 and cube3[3][4]==6) or (cube3[1][6]==6 and cube3[3][4]==1):
        print("impossible")
    if (cube3[1][6]==3 and cube3[3][4]==5) or (cube3[1][6]==5 and cube3[3][4]==3):
        print("impossible")
    if (cube3[1][6]==2 and cube3[3][4]==4) or (cube3[1][6]==4 and cube3[3][4]==2):
        print("impossible")

    if (cube3[2][6]==1 and cube3[3][5]==6) or (cube3[2][6]==6 and cube3[3][5]==1):
        print("impossible")
    if (cube3[2][6]==3 and cube3[3][5]==5) or (cube3[2][6]==5 and cube3[3][5]==3):
        print("impossible")
    if (cube3[2][6]==2 and cube3[3][5]==4) or (cube3[2][6]==4 and cube3[3][5]==2):
        print("impossible")

    if (cube3[3][2]==1 and cube3[3][3]==6) or (cube3[3][2]==6 and cube3[3][3]==1):
        print("impossible")
    if (cube3[3][2]==3 and cube3[3][3]==5) or (cube3[3][2]==5 and cube3[3][3]==3):
        print("impossible")
    if (cube3[3][2]==2 and cube3[3][3]==4) or (cube3[3][2]==4 and cube3[3][3]==2):
        print("impossible")

    if (cube3[3][6]==1 and cube3[3][5]==6) or (cube3[3][6]==6 and cube3[3][5]==1):
        print("impossible")
    if (cube3[3][6]==3 and cube3[3][5]==5) or (cube3[3][6]==5 and cube3[3][5]==3):
        print("impossible")
    if (cube3[3][6]==2 and cube3[3][5]==4) or (cube3[3][6]==4 and cube3[3][5]==2):
        print("impossible")

    if (cube3[2][6]==1 and cube3[3][6]==6) or (cube3[2][6]==6 and cube3[3][6]==1):
        print("impossible")
    if (cube3[2][6]==3 and cube3[3][6]==5) or (cube3[2][6]==5 and cube3[3][6]==3):
        print("impossible")
    if (cube3[2][6]==2 and cube3[3][6]==4) or (cube3[2][6]==4 and cube3[3][6]==2):
        print("impossible")

    if (cube3[4][6]==1 and cube3[4][5]==6) or (cube3[4][6]==6 and cube3[4][5]==1):
        print("impossible")
    if (cube3[4][6]==3 and cube3[4][5]==5) or (cube3[4][6]==5 and cube3[4][5]==3):
        print("impossible")
    if (cube3[4][6]==2 and cube3[4][5]==4) or (cube3[4][6]==4 and cube3[4][5]==2):
        print("impossible")

    if (cube3[4][2]==1 and cube3[4][3]==6) or (cube3[4][2]==6 and cube3[4][3]==1):
        print("impossible")
    if (cube3[4][2]==3 and cube3[4][3]==5) or (cube3[4][2]==5 and cube3[4][3]==3):
        print("impossible")
    if (cube3[4][2]==2 and cube3[4][3]==4) or (cube3[4][2]==4 and cube3[4][3]==2):
        print("impossible")

    if (cube3[0][7]==1 and cube3[3][1]==6) or (cube3[0][7]==6 and cube3[3][1]==1):
        print("impossible")
    if (cube3[0][7]==3 and cube3[3][1]==5) or (cube3[0][7]==5 and cube3[3][1]==3):
        print("impossible")
    if (cube3[0][7]==2 and cube3[3][1]==4) or (cube3[0][7]==4 and cube3[3][1]==2):
        print("impossible")

    if (cube3[0][8]==1 and cube3[3][0]==6) or (cube3[0][8]==6 and cube3[3][0]==1):
        print("impossible")
    if (cube3[0][8]==3 and cube3[3][0]==5) or (cube3[0][8]==5 and cube3[3][0]==3):
        print("impossible")
    if (cube3[0][8]==2 and cube3[3][0]==4) or (cube3[0][8]==4 and cube3[3][0]==2):
        print("impossible")

    if (cube3[0][8]==1 and cube3[3][11]==6) or (cube3[0][8]==6 and cube3[3][11]==1):
        print("impossible")
    if (cube3[0][8]==3 and cube3[3][11]==5) or (cube3[0][8]==5 and cube3[3][11]==3):
        print("impossible")
    if (cube3[0][8]==2 and cube3[3][11]==4) or (cube3[0][8]==4 and cube3[3][11]==2):
        print("impossible")


    if (cube3[1][8]==1 and cube3[4][10]==6) or (cube3[1][8]==6 and cube3[4][10]==1):
        print("impossible")
    if (cube3[1][8]==3 and cube3[4][10]==5) or (cube3[1][8]==5 and cube3[4][10]==3):
        print("impossible")
    if (cube3[1][8]==2 and cube3[4][10]==4) or (cube3[1][8]==4 and cube3[4][10]==2):
        print("impossible")

    if (cube3[3][0]==1 and cube3[3][11]==6) or (cube3[3][0]==6 and cube3[3][11]==1):
        print("impossible")
    if (cube3[3][0]==3 and cube3[3][11]==5) or (cube3[3][0]==5 and cube3[3][11]==3):
        print("impossible")
    if (cube3[3][0]==2 and cube3[3][11]==4) or (cube3[3][0]==4 and cube3[3][11]==2):
        print("impossible")

    if (cube3[2][8]==1 and cube3[3][9]==6) or (cube3[2][8]==6 and cube3[3][9]==1):
        print("impossible")
    if (cube3[2][8]==3 and cube3[3][9]==5) or (cube3[2][8]==5 and cube3[3][9]==3):
        print("impossible")
    if (cube3[2][8]==2 and cube3[3][9]==4) or (cube3[2][8]==4 and cube3[3][9]==2):
        print("impossible")

    if (cube3[2][8]==1 and cube3[3][8]==6) or (cube3[2][8]==6 and cube3[3][8]==1):
        print("impossible")
    if (cube3[2][8]==3 and cube3[3][8]==5) or (cube3[2][8]==5 and cube3[3][8]==3):
        print("impossible")
    if (cube3[2][8]==2 and cube3[3][8]==4) or (cube3[2][8]==4 and cube3[3][8]==2):
        print("impossible")

    if (cube3[3][8]==1 and cube3[3][9]==6) or (cube3[3][8]==6 and cube3[3][9]==1):
        print("impossible")
    if (cube3[3][8]==3 and cube3[3][9]==5) or (cube3[3][8]==5 and cube3[3][9]==3):
        print("impossible")
    if (cube3[3][8]==2 and cube3[3][9]==4) or (cube3[3][8]==4 and cube3[3][9]==2):
        print("impossible")

    if (cube3[4][8]==1 and cube3[4][9]==6) or (cube3[4][8]==6 and cube3[4][9]==1):
        print("impossible")
    if (cube3[4][8]==3 and cube3[4][9]==5) or (cube3[4][8]==5 and cube3[4][9]==3):
        print("impossible")
    if (cube3[4][8]==2 and cube3[4][9]==4) or (cube3[4][8]==4 and cube3[4][9]==2):
        print("impossible")

    if (cube3[4][11]==1 and cube3[4][0]==6) or (cube3[4][11]==6 and cube3[4][0]==1):
        print("impossible")
    if (cube3[4][11]==3 and cube3[4][0]==5) or (cube3[4][11]==5 and cube3[4][0]==3):
        print("impossible")
    if (cube3[4][11]==2 and cube3[4][0]==4) or (cube3[4][11]==4 and cube3[4][0]==2):
        print("impossible")

    if (cube3[2][7]==1 and cube3[3][7]==6) or (cube3[2][7]==6 and cube3[3][7]==1):
        print("impossible")
    if (cube3[2][7]==3 and cube3[3][7]==5) or (cube3[2][7]==5 and cube3[3][7]==3):
        print("impossible")
    if (cube3[2][7]==2 and cube3[3][7]==4) or (cube3[2][7]==4 and cube3[3][7]==2):
        print("impossible")

    if (cube3[5][7]==1 and cube3[6][7]==6) or (cube3[5][7]==6 and cube3[6][7]==1):
        print("impossible")
    if (cube3[5][7]==3 and cube3[6][7]==5) or (cube3[5][7]==5 and cube3[6][7]==3):
        print("impossible")
    if (cube3[5][7]==2 and cube3[6][7]==4) or (cube3[5][7]==4 and cube3[6][7]==2):
        print("impossible")


    if numrouge==9 and numbleu==9 and numvert==9 and numjaune==9 and numblanc==9 and numorange==9:

        text=Label(fenetre,text="Le Rubik's cube peut être résolu",fg='blue',bg="grey90")
        text.place(x=370,y=80)
        text.after(4000,text.destroy)


def solve_croix():
    combinaison=[]
    global cube3
    #met la premieere case en blanc
    if cube3[3][7]!=4:
        if cube3[3][7]!=4 and cube3[3][1]==4:
            text=Label(fenetre,text="Ui*2",fg='blue',bg="grey90")
            text.place(x=370,y=80)
            text.after(3000,text.destroy)
            U_prime(),U_prime()


        if cube3[3][7]!=4 and cube3[3][4]==4:
            combinaison.append("Ui")
            U_prime()

        if cube3[3][7]!=4 and cube3[3][10]==4:
            combinaison.append("U")
            U()

        if cube3[3][7]!=4 and cube3[1][6]==4:
            combinaison.append("Fi L F")
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[1][8]==4:
            combinaison.append("Ui Fi L F")
            U_prime()
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[0][7]==4:
            combinaison.append("Ui F Ri Fi")
            U_prime()
            F()
            R_prime()
            F_prime()

        if cube3[3][7]!=4 and cube3[4][9]==4:
            combinaison.append("R U")
            R()
            U()

        if cube3[3][7]!=4 and cube3[4][11]==4:
            combinaison.append("Ri U")
            R_prime()
            U()

        if cube3[3][7]!=4 and cube3[4][5]==4:
            combinaison.append("Li Ui")
            L_prime()
            U_prime()

        if cube3[3][7]!=4 and cube3[4][3]==4:
            combinaison.append("Bi Ui Fi L F ")
            B_prime()
            U_prime()
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[4][2]==4:
            combinaison.append("Bi U U")
            B_prime()
            U()
            U()

        if cube3[3][7]!=4 and cube3[4][0]==4:
            combinaison.append("B U U")
            B()
            U()
            U()

        if cube3[3][7]!=4 and cube3[5][1]==4:
            combinaison.append("B B U U")
            B()
            B()
            U()
            U()

        if cube3[3][7]!=4 and cube3[5][4]==4:
            combinaison.append("Di B B U U")
            D_prime()
            B()
            B()
            U()
            U()

        if cube3[3][7]!=4 and cube3[5][10]==4:
            combinaison.append("D B B U U")
            D()
            B()
            B()
            U()
            U()

        if cube3[3][7]!=4 and cube3[6][7]==4:
            combinaison.append("D D B B Ui Fi L F")
            D()
            D()
            B()
            B()
            U_prime()
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[7][6]==4:
            combinaison.append("Di B B Ui Fi L F")
            D_prime()
            B()
            B()
            U_prime()
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[7][8]==4:
            combinaison.append("D  B B Ui Fi L F")
            D()
            B()
            B()
            U_prime()
            F_prime()
            L()
            F()

        if cube3[3][7]!=4 and cube3[8][7]==4:
            combinaison.append("B B U Ui Fi L F")
            B()
            B()
            U()
            U_prime()
            F_prime()
            L()
            F()
    #Pour mettre la seconde case du centre en blanc:
    elif cube3[4][8]!=4 :
        if cube3[4][8]!=4 and cube3[1][8]==4:
            combinaison.append("Ri")
            R_prime()

        if cube3[4][8]!=4 and cube3[7][8]==4:
            combinaison.append("R")
            R()

        if cube3[4][8]!=4 and cube3[4][0]==4:
            combinaison.append("R R")
            R()
            R()

        if cube3[4][8]!=4 and cube3[4][2]==4:
            combinaison.append("Bi Bi R R")
            B_prime()
            B_prime()
            R()
            R()

        if cube3[4][8]!=4 and cube3[3][1]==4:
            combinaison.append("Bi R R")
            B_prime()
            R()
            R()

        if cube3[4][8]!=4 and cube3[5][1]==4:
            combinaison.append("B R R")
            B()
            R()
            R()

        if cube3[4][8]!=4 and cube3[4][3]==4:
            combinaison.append("B B R F Di Fi ")
            B()
            B()
            R()
            F()
            D_prime()
            F_prime()
        

        if cube3[4][8]!=4 and cube3[4][5]==4:
            combinaison.append("Li Fi Ui F")
            L_prime()
            F_prime()
            U_prime()
            F()

        if cube3[4][8]!=4 and cube3[3][4]==4:
            combinaison.append("Fi Ui F")
            F_prime()
            U_prime()
            F()

        if cube3[4][8]!=4 and cube3[5][4]==4:
            combinaison.append("L L Fi Ui F")
            L()
            L()
            F_prime()
            U_prime()
            F()

        if cube3[4][8]!=4 and cube3[1][6]==4:
            combinaison.append("L L D D Li")
            L()
            L()
            D()
            D()
            L_prime()

        if cube3[4][8]!=4 and cube3[0][7]==4:
            combinaison.append("U Ri Ui")
            U()
            R_prime()
            U_prime()

        if cube3[4][8]!=4 and cube3[4][9]==4:
            combinaison.append("Ri F Di Fi")
            R_prime()
            F()
            D_prime()
            F_prime()

        if cube3[4][8]!=4 and cube3[3][10]==4:
            combinaison.append("Fi U F")
            F_prime()
            U()
            F()

        if cube3[4][8]!=4 and cube3[4][11]==4:
            combinaison.append("Bi Di U R Ui")
            B_prime()
            D_prime()
            U()
            R()
            U_prime()

        if cube3[4][8]!=4 and cube3[5][10]==4:
            combinaison.append("F Di Fi")
            F()
            D_prime()
            F_prime()

        if cube3[4][8]!=4 and cube3[6][7]==4:
            combinaison.append("D R")
            D()
            R()

        if cube3[4][8]!=4 and cube3[7][6]==4:
            combinaison.append("D D R")
            D()
            D()
            R()

        if cube3[4][8]!=4 and cube3[8][8]==4:
            combinaison.append("Di R")
            D_prime()
            R()
            
    #Maintenant, la troisème face du centre en blanc:
    elif cube3[5][7]!=4 :
        if cube3[5][7]!=4 and cube3[5][10]==4:
            combinaison.append("Di")
            D_prime()

        if cube3[5][7]!=4 and cube3[5][4]==4:
            combinaison.append("D")
            D()

        if cube3[5][7]!=4 and cube3[5][1]==4:
            combinaison.append("D D")
            D()
            D()

        if cube3[5][7]!=4 and cube3[4][11]==4:
            combinaison.append("R Di Ri")
            R()
            D_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[3][10]==4:
            combinaison.append("R Di Ri")
            R()
            D_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[3][10]==4:
            combinaison.append("R R Di Ri Ri")
            R()
            R()
            D_prime()
            R_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[6][7]==4:
            combinaison.append("D D B R Di Ri")
            D()
            D()
            B()
            R()
            D_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[7][8]==4:
            combinaison.append("D B R Di Ri")
            D()
            B()
            R()
            D_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[7][6]==4:
            combinaison.append("Di B R Di Ri")
            D_prime()
            B()
            R()
            D_prime()
            R_prime()

        if cube3[5][7]!=4 and cube3[8][7]==4:
            combinaison.append("B R Di Ri")
            B()
            R()
            D_prime()

        if cube3[5][7]!=4 and cube3[4][5]==4:
            combinaison.append("L D")
            L()
            D()

        if cube3[5][7]!=4 and cube3[4][3]==4:
            combinaison.append("Li D")
            L_prime()
            D()

        if cube3[5][7]!=4 and cube3[3][4]==4:
            combinaison.append("Li Li D")
            L_prime()
            L_prime()
            D()

        if cube3[5][7]!=4 and cube3[3][1]==4:
            combinaison.append("B B D D")
            B()
            B()
            D()
            D()

        if cube3[5][7]!=4 and cube3[4][0]==4:
            combinaison.append("Bi Di Di")
            B_prime()
            D_prime()
            D_prime()

        if cube3[5][7]!=4 and cube3[1][6]==4:
            combinaison.append("F L Fi")
            F()
            L()
            F_prime()

        if cube3[5][7]!=4 and cube3[1][8]==4:
            combinaison.append("Fi L F")
            F_prime()
            L()
            F()

        if cube3[5][7]!=4 and cube3[1][8]==4:
            combinaison.append("Ui F L Fi U")
            U_prime()
            F()
            L()
            F_prime()
            U()
    #Et maintenant, la dernière case blanche de la face du milieu
    elif cube3[4][6]!=4 :
        if cube3[4][6]!=4 and cube3[1][6]==4:
            combinaison.append("L")
            L()

        if cube3[4][6]!=4 and cube3[7][6]==4:
            combinaison.append("Li")
            L_prime()

        if cube3[4][6]!=4 and cube3[4][0]==4:
            combinaison.append("L L")
            L()
            L()

        if cube3[4][6]!=4 and cube3[0][7]==4:
            combinaison.append("Ui L U")
            U_prime()
            L()
            U()

        if cube3[4][6]!=4 and cube3[1][8]==4:
            combinaison.append("U U L Ui Ui")
            U()
            U()
            L()
            U_prime()
            U_prime()

        if cube3[4][6]!=4 and cube3[8][7]==4:
            combinaison.append("D Li")
            D()
            L_prime()

        if cube3[4][6]!=4 and cube3[7][8]==4:
            combinaison.append("D D Li Di Di")
            D()
            D()
            L_prime()
            D_prime()
            D_prime()

        if cube3[4][6]!=4 and cube3[3][1]==4:
            combinaison.append("B L L")
            B()
            L()
            L()

        if cube3[4][6]!=4 and cube3[5][1]==4:
            combinaison.append("Bi L L")
            B_prime()
            L()
            L()

        if cube3[4][6]!=4 and cube3[4][2]==4:
            combinaison.append("B B L L")
            B()
            B()
            L()
            L()

        if cube3[4][6]!=4 and cube3[4][3]==4:
            combinaison.append("D Ui Bi Ui L Ui Ui Di")
            D()
            U_prime()
            B_prime()
            U_prime()
            L()
            U_prime()
            U_prime()
            D_prime()

        if cube3[4][6]!=4 and cube3[3][4]==4:
            combinaison.append("Li D Ui Bi Ui L Ui Ui Di")
            L_prime()
            D()
            U_prime()
            B_prime()
            U_prime()
            L()
            U_prime()
            U_prime()
            D_prime()

        if cube3[4][6]!=4 and cube3[5][4]==4:
            combinaison.append("L D Ui Bi Ui L Ui Ui Di")
            L(),D(),U_prime()
            B_prime()
            U_prime()
            L()
            U_prime()
            U_prime()
            D_prime()

        if cube3[4][6]!=4 and cube3[4][5]==4:
            combinaison.append("L L D Ui Bi Ui L Ui Ui Di")
            L()
            L()
            D()
            U_prime()
            B_prime()
            U_prime()
            L()
            U_prime()
            U_prime()
            D_prime()

        if cube3[4][6]!=4 and cube3[3][10]==4:
            combinaison.append("F U Fi")
            F()
            U()
            F_prime()

        if cube3[4][6]!=4 and cube3[5][10]==4:
            combinaison.append("Fi Di F")
            F_prime()
            D_prime()
            F()

        if cube3[4][6]!=4 and cube3[5][10]==4:
            combinaison.append("Ri F U Fi R")
            R_prime()
            F()
            U()
            F_prime()
            R()

    #Regarder si la croix est au bon endroit

    elif cube3[2][7]!=1:
        if cube3[4][9]==1:
            combinaison.append("R R U U B U U B B R R")
        if cube3[4][5]==1:
            combinaison.append("U U L L B L L B B U U")
        if cube3[6][7]==1:
            combinaison.append("U U D D B B U U D D")

    elif cube3[4][9]!=3:
        if cube3[4][5]==3:
            combinaison.append("L L R R B B R R L L")
        if cube3[6][7]==3:
            combinaison.append("R R D D Bi D D B B R R")

    elif cube3[4][5]!=5:
        if cube3[6][7]==5:
            combinaison.append("D D L L Bi L L B B D D")
    else:
        print("c'est bon")

    print(combinaison)


def solve_faceblanche():
    global cube3
    combinaison=[]
    #Pour que l'angle haut droit de la face blanche soit le blanc bleu et orange
    if (cube3[3][6]!=4 and cube3[3][5]!=5 and cube3[2][6]!=1) or (cube3[3][6]!=4 and cube3[3][5]!=1 and cube3[2][6]!=4) or (cube3[3][6]!=1 and cube3[3][5]!=4 and cube3[2][6]!=5): #angle bleu-orange-blanc
        if (cube3[3][8]==4 and cube3[2][8]==5 and cube3[3][9]==1) or (cube3[3][8]==5 and cube3[2][8]==1 and cube3[3][9]==4) or (cube3[3][8]==1 and cube3[2][8]==4 and cube3[3][9]==5):
            combinaison.append("R B Ri B U Bi Ui Bi Ui B U ")
        if cube3[5][8]==4 and cube3[6][8]==5 and cube3[5][9]==1 or cube3[5][8]==5 and cube3[6][8]==1 and cube3[5][9]==4 or cube3[5][8]==1 and cube3[6][8]==4 and cube3[5][9]==5:
            combinaison.append("Ri B R Bi U Bi Ui B Ri B R")
        if cube3[5][5]==4 and cube3[5][6]==5 and cube3[6][6]==1 or cube3[5][5]==5 and cube3[5][6]==1 and cube3[6][6]==4 or cube3[5][5]==1 and cube3[5][6]==4 and cube3[6][6]==5:
            combinaison.append("U B Ui B L Bi Li Bi Bi Li B L ")

        if cube3[5][2]==4 and cube3[5][3]==5 and cube3[8][6]==1 or cube3[5][2]==5 and cube3[5][3]==1 and cube3[8][6]==4 or cube3[5][2]==1 and cube3[5][3]==4 and cube3[8][6]==5:
            combinaison.append("U Bi Ui")
        if cube3[5][0]==4 and cube3[5][11]==5 and cube3[8][8]==1 or cube3[5][0]==5 and cube3[5][11]==1 and cube3[8][8]==4 or cube3[5][0]==1 and cube3[5][11]==4 and cube3[8][8]==5:
            combinaison.append("B Li B L")
        if cube3[3][0]==4 and cube3[3][11]==5 and cube3[0][8]==1 or cube3[3][0]==5 and cube3[3][11]==1 and cube3[0][8]==4 or cube3[3][0]==1 and cube3[3][11]==4 and cube3[0][8]==5:
            combinaison.append("Li B L")
        if cube3[3][2]==4 and cube3[3][3]==5 and cube3[0][6]==1 or cube3[3][2]==5 and cube3[3][3]==1 and cube3[0][6]==4 or cube3[3][2]==1 and cube3[3][3]==4 and cube3[0][6]==5:
            combinaison.append("Bi Li B L")


    elif cube3[3][8]!=4 and cube3[2][8]!=3 and cube3[3][9]!=1 or cube3[3][8]!=3 and cube3[2][8]!=1 and cube3[3][9]!=4 or cube3[3][8]!=1 and cube3[2][8]!=4 and cube3[3][9]!=3:
        if cube3[5][8]==4 and cube3[6][8]==3 and cube3[5][9]==1 or cube3[5][8]==3 and cube3[6][8]==1 and cube3[5][9]==4 or cube3[5][8]==1 and cube3[6][8]==4 and cube3[5][9]==3:
            combinaison.append("D B Di B R Bi Ri Bi Bi Ri B R ")
        if cube3[5][5]==4 and cube3[5][6]==3 and cube3[6][6]==1 or cube3[5][5]==3 and cube3[5][6]==1 and cube3[6][6]==4 or cube3[5][5]==1 and cube3[5][6]==4 and cube3[6][6]==3:
            combinaison.append("Di Bi D Bi Ui B U Bi L Bi Li")

        if cube3[5][2]==4 and cube3[5][3]==3 and cube3[8][6]==1 or cube3[5][2]==3 and cube3[5][3]==1 and cube3[8][6]==4 or cube3[5][2]==1 and cube3[5][3]==4 and cube3[8][6]==3:
            combinaison.append("Bi R Bi Ri")
        if cube3[5][0]==4 and cube3[5][11]==3 and cube3[8][8]==1 or cube3[5][0]==3 and cube3[5][11]==1 and cube3[8][8]==4 or cube3[5][0]==1 and cube3[5][11]==4 and cube3[8][8]==3:
            combinaison.append("Bi R B B Ri")
        if cube3[3][0]==4 and cube3[3][11]==3 and cube3[0][8]==1 or cube3[3][0]==3 and cube3[3][11]==1 and cube3[0][8]==4 or cube3[3][0]==1 and cube3[3][11]==4 and cube3[0][8]==3:
            combinaison.append("B R Bi Ri")
        if cube3[3][2]==4 and cube3[3][3]==3 and cube3[0][6]==1 or cube3[3][2]==3 and cube3[3][3]==1 and cube3[0][6]==4 or cube3[3][2]==1 and cube3[3][3]==4 and cube3[0][6]==3:
            combinaison.append("R Bi Ri")


    elif cube3[5][8]!=4 and cube3[6][8]!=3 and cube3[5][9]!=6 or cube3[5][8]!=3 and cube3[6][8]!=6 and cube3[5][9]!=4 or cube3[5][8]!=6 and cube3[6][8]!=4 and cube3[5][9]!=3:
        if cube3[5][5]==4 and cube3[5][6]==3 and cube3[6][6]==6 or cube3[5][5]==3 and cube3[5][6]==6 and cube3[6][6]==4 or cube3[5][5]==6 and cube3[5][6]==4 and cube3[6][6]==3:
            combinaison.append("L B Li B D Bi Di Bi Bi Di B D")

        if cube3[5][2]==4 and cube3[5][3]==3 and cube3[8][6]==6 or cube3[5][2]==3 and cube3[5][3]==6 and cube3[8][6]==4 or cube3[5][2]==6 and cube3[5][3]==4 and cube3[8][6]==3:
            combinaison.append("Ri B R")
        if cube3[5][0]==4 and cube3[5][11]==3 and cube3[8][8]==6 or cube3[5][0]==3 and cube3[5][11]==6 and cube3[8][8]==4 or cube3[5][0]==6 and cube3[5][11]==4 and cube3[8][8]==3:
            combinaison.append("Bi Ri B R")
        if cube3[3][0]==4 and cube3[3][11]==3 and cube3[0][8]==6 or cube3[3][0]==3 and cube3[3][11]==6 and cube3[0][8]==4 or cube3[3][0]==6 and cube3[3][11]==4 and cube3[0][8]==3:
            combinaison.append("B R Bi Bi Ri")
        if cube3[3][2]==4 and cube3[3][3]==3 and cube3[0][6]==6 or cube3[3][2]==3 and cube3[3][3]==6 and cube3[0][6]==4 or cube3[3][2]==6 and cube3[3][3]==4 and cube3[0][6]==3:
            combinaison.append("Ri Bi Bi R")

    elif cube3[5][5]!=5 and cube3[5][6]!=4 and cube3[6][6]!=6 or cube3[5][5]!=4 and cube3[5][6]!=6 and cube3[6][6]!=5 or cube3[5][5]!=6 and cube3[5][6]!=5 and cube3[6][6]!=4:
        if cube3[5][2]==5 and cube3[5][3]==4 and cube3[8][6]==6 or cube3[5][2]==4 and cube3[5][3]==6 and cube3[8][6]==5 or cube3[5][2]==6 and cube3[5][3]==5 and cube3[8][6]==4:
            combinaison.append("B R Bi Ri")
        if cube3[5][0]==5 and cube3[5][11]==4 and cube3[8][8]==6 or cube3[5][0]==4 and cube3[5][11]==6 and cube3[8][8]==5 or cube3[5][0]==6 and cube3[5][11]==5 and cube3[8][8]==4:
            combinaison.append("Ri Bi R")
        if cube3[3][0]==5 and cube3[3][11]==4 and cube3[0][8]==6 or cube3[3][0]==4 and cube3[3][11]==6 and cube3[0][8]==5 or cube3[3][0]==6 and cube3[3][11]==5 and cube3[0][8]==4:
            combinaison.append("Bi Ri Bi R")
        if cube3[3][2]==5 and cube3[3][3]==4 and cube3[0][6]==6 or cube3[3][2]==4 and cube3[3][3]==6 and cube3[0][6]==5 or cube3[3][2]==6 and cube3[3][3]==5 and cube3[0][6]==4:
            combinaison.append("B B Ri Bi R")

    elif cube3[3][6]==4 and cube3[3][8]==4 and cube3[5][6]==4 and cube3[5][8]==4:
        cominaison.apprend("4")
    print(combinaison)


"""
On définit chaques mouvement: 
    R = Right , Ri = Right inversé
    B = Back  , Bi = Back inversé
    D = Down  , Di = Down inversé
    F = Front , Fi = Front inversé
    L = Left  , Li = Left inversé
    U = Up    , Ui = Up inversé

"""
def R():
    #Définie le mouvement Right (Droite)
    global cube3
    x=cube3[0][8]
    v=cube3[1][8]
    w=cube3[2][8]
    a=cube3[3][9]
    b=cube3[4][9]
    c=cube3[5][9]
    d=cube3[3][10]
    cube3[0][8]=cube3[3][8]
    cube3[1][8]=cube3[4][8]
    cube3[2][8]=cube3[5][8]
    cube3[3][8]=cube3[6][8]
    cube3[4][8]=cube3[7][8]
    cube3[5][8]=cube3[8][8]
    cube3[6][8]=cube3[3][0]
    cube3[7][8]=cube3[4][0]
    cube3[8][8]=cube3[5][0]
    cube3[3][0]=w
    cube3[4][0]=v
    cube3[5][0]=x
    cube3[4][9]=cube3[5][10]
    cube3[5][9]=cube3[5][11]
    cube3[5][10]=cube3[4][11]
    cube3[5][11]=cube3[3][11]
    cube3[3][11]=a
    cube3[3][10]=b
    cube3[3][9]=c
    cube3[4][11]=d
    afficher(cube3)

def R_prime():
    #Définie le mouvement Right (Droite) inversé
    x=cube3[5][0]
    v=cube3[4][0]
    w=cube3[3][0]
    a=cube3[3][9]
    b=cube3[4][9]
    c=cube3[5][9]
    cube3[5][0]=cube3[6][8]
    cube3[4][0]=cube3[7][8]
    cube3[3][0]=cube3[8][8]
    cube3[6][8]=cube3[3][8]
    cube3[7][8]=cube3[4][8]
    cube3[8][8]=cube3[5][8]
    cube3[3][8]=cube3[0][8]
    cube3[4][8]=cube3[1][8]
    cube3[5][8]=cube3[2][8]
    cube3[0][8]=x
    cube3[1][8]=v
    cube3[2][8]=w
    cube3[3][10]=cube3[4][11]
    cube3[3][9]=cube3[3][11]
    cube3[3][11]=cube3[5][11]
    cube3[4][11]=cube3[5][10]
    cube3[5][11]=c
    cube3[5][10]=b
    cube3[5][9]=a
    afficher(cube3)

def B():
    #Définie le mouvement Back (Derrière)
    xx=cube3[0][6]
    yy=cube3[0][7]
    zz=cube3[0][8]
    cube3[0][6]=cube3[3][11]
    cube3[0][7]=cube3[4][11]
    cube3[0][8]=cube3[5][11]
    cube3[3][11]=cube3[8][8]
    cube3[4][11]=cube3[8][7]
    cube3[5][11]=cube3[8][6]
    cube3[8][8]=cube3[5][3]
    cube3[8][7]=cube3[4][3]
    cube3[8][6]=cube3[3][3]
    cube3[5][3]=xx
    cube3[4][3]=yy
    cube3[3][3]=zz
    a=cube3[3][0]
    b=cube3[3][1]
    c=cube3[3][2]
    cube3[3][0]=c
    cube3[3][1]=cube3[4][2]
    cube3[3][2]=cube3[5][2]
    cube3[4][2]=cube3[5][1]
    cube3[5][2]=cube3[5][0]
    cube3[5][1]=cube3[4][0]
    cube3[5][0]=a
    cube3[4][0]=b
    afficher(cube3)

def B_prime():
    #Définie le mouvement Back (Derrière) inversé
    aa=cube3[0][6]
    bb=cube3[0][7]
    cc=cube3[0][8]
    cube3[0][6]=cube3[5][3]
    cube3[0][7]=cube3[4][3]
    cube3[0][8]=cube3[3][3]
    cube3[5][3]=cube3[8][8]
    cube3[4][3]=cube3[8][7]
    cube3[3][3]=cube3[8][6] 
    cube3[8][8]=cube3[3][11]
    cube3[8][7]=cube3[4][11]
    cube3[8][6]=cube3[5][11] 
    cube3[3][11]=aa
    cube3[4][11]=bb
    cube3[5][11]=cc
    xx=cube3[3][0]
    yy=cube3[3][1]
    cube3[3][0]=cube3[3][2]
    cube3[3][1]=cube3[4][2]
    cube3[3][2]=cube3[5][2]
    cube3[4][2]=cube3[5][1]  
    cube3[5][2]=cube3[5][0]
    cube3[5][1]=cube3[4][0]
    cube3[5][0]=xx
    cube3[4][0]=yy
    afficher(cube3)

def U():
    #Définie le mouvement Up (Haut)
    aa=cube3[3][6]
    bb=cube3[3][7]
    cc=cube3[3][8]
    cube3[3][6]=cube3[3][9]
    cube3[3][7]=cube3[3][10]
    cube3[3][8]=cube3[3][11]       
    cube3[3][9]=cube3[3][0] 
    cube3[3][10]=cube3[3][1] 
    cube3[3][11]=cube3[3][2]  
    cube3[3][0]=cube3[3][3]
    cube3[3][1]=cube3[3][4]
    cube3[3][2]=cube3[3][5]
    cube3[3][3]=aa
    cube3[3][4]=bb
    cube3[3][5]=cc
    xx=cube3[2][6]
    yy=cube3[2][7]
    cube3[2][6]=cube3[2][8]
    cube3[2][7]=cube3[1][8]
    cube3[2][8]=cube3[0][8]
    cube3[1][8]=cube3[0][7]
    cube3[0][8]=cube3[0][6]
    cube3[0][7]= cube3[1][6]   
    cube3[0][6]=xx
    cube3[1][6]=yy  
    afficher(cube3)
    
def U_prime():
    #Définie le mouvement Up (Haut) inversé


    aa=cube3[3][6]
    bb=cube3[3][7]
    cc=cube3[3][8]
    cube3[3][6]=cube3[3][3]
    cube3[3][7]=cube3[3][4]
    cube3[3][8]=cube3[3][5]
    cube3[3][3]=cube3[3][0]
    cube3[3][4]=cube3[3][1]
    cube3[3][5]=cube3[3][2]
    cube3[3][0]=cube3[3][9]
    cube3[3][1]=cube3[3][10]
    cube3[3][2]=cube3[3][11]
    cube3[3][9]=aa
    cube3[3][10]=bb
    cube3[3][11]=cc
    yy=cube3[2][6]
    xx=cube3[2][7]
    cube3[2][6]=cube3[0][6]
    cube3[2][7]=cube3[1][6]   
    cube3[0][6]=cube3[0][8]
    cube3[1][6]=cube3[0][7]
    cube3[0][8]=cube3[2][8]
    cube3[0][7]=cube3[1][8]   
    cube3[2][8]=yy
    cube3[1][8]=xx

    afficher(cube3)   
    
    
    
    
def F_prime():
    #Définie le mouvement Front (Face) inversé
    a=cube3[3][6]
    b=cube3[3][7]
    c=cube3[3][8]
    d=cube3[2][6]
    e=cube3[2][7]
    f=cube3[2][8]
    cube3[3][8]=cube3[5][8]
    cube3[5][8]=cube3[5][6]
    cube3[3][7]=cube3[4][8]
    cube3[4][8]=cube3[5][7]
    cube3[5][6]=cube3[5][8]
    cube3[5][7]=cube3[4][6]
    cube3[3][6]=c
    cube3[4][6]=b
    cube3[5][6]=a
    cube3[2][6]=cube3[3][9]
    cube3[2][7]=cube3[4][9]
    cube3[2][8]=cube3[5][9]
    cube3[3][9]=cube3[6][8]
    cube3[4][9]=cube3[6][7]
    cube3[5][9]=cube3[6][6]
    cube3[6][8]=cube3[5][5]
    cube3[6][7]=cube3[4][5]
    cube3[6][6]=cube3[3][5]
    cube3[3][5]=f
    cube3[4][5]=e
    cube3[5][5]=d
    afficher(cube3)

def F():
    #Définie le mouvement Front (Face)
    a=cube3[2][6]
    b=cube3[2][7]
    c=cube3[2][8]
    d=cube3[3][6]
    e=cube3[3][7]
    f=cube3[3][8]
    cube3[2][6]=cube3[5][5]
    cube3[2][7]=cube3[4][5]
    cube3[2][8]=cube3[3][5]
    cube3[5][5]=cube3[6][8]
    cube3[4][5]=cube3[6][7]
    cube3[3][5]=cube3[6][6]
    cube3[6][8]=cube3[3][9]
    cube3[6][7]=cube3[4][9]
    cube3[6][6]=cube3[5][9]
    cube3[3][9]=a
    cube3[4][9]=b
    cube3[5][9]=c
    cube3[3][6]=cube3[5][6]
    cube3[3][7]=cube3[4][6]
    cube3[3][8]=cube3[3][6]
    cube3[4][6]=cube3[4][7]
    cube3[5][6]=cube3[5][8]
    cube3[5][7]=cube3[4][8]
    cube3[5][8]=f
    cube3[4][8]=e
    cube3[3][8]=d
    afficher(cube3)

def L():
    #Définie le mouvement Left (Gauche)
    xx=cube3[3][6]
    yy=cube3[4][6]
    zz=cube3[5][6]
    cube3[3][6]=cube3[0][6]
    cube3[4][6]=cube3[1][6]
    cube3[5][6]=cube3[2][6]
    cube3[0][6]=cube3[5][2]
    cube3[1][6]=cube3[4][2]
    cube3[2][6]=cube3[3][2]
    cube3[5][2]=cube3[6][6]
    cube3[4][2]=cube3[7][6]
    cube3[3][2]=cube3[8][6]
    cube3[6][6]=xx
    cube3[7][6]=yy
    cube3[8][6]=zz
    aa=cube3[3][3]
    bb=cube3[3][4]
    cube3[3][3]=cube3[5][3]
    cube3[3][4]=cube3[4][3]
    cube3[5][3]=cube3[5][5]
    cube3[4][3]=cube3[5][4]
    cube3[5][5]=cube3[3][5]
    cube3[5][4]=cube3[4][5]
    cube3[3][5]=aa
    cube3[4][5]=bb
    afficher(cube3)
    
def L_prime():
    #Définie le mouvement Left (Gauche) inversé
    xx=cube3[3][6]
    yy=cube3[4][6]
    zz=cube3[5][6]
    cube3[3][6]=cube3[6][6]
    cube3[4][6]=cube3[7][6]
    cube3[5][6]=cube3[8][6]
    cube3[6][6]=cube3[5][2]
    cube3[7][6]=cube3[4][2]
    cube3[8][6]=cube3[3][2]
    cube3[5][2]=cube3[0][6]
    cube3[4][2]=cube3[1][6]
    cube3[3][2]=cube3[2][6] 
    cube3[0][6]=xx
    cube3[1][6]=yy
    cube3[2][6]=zz
    aa=cube3[3][3]
    bb=cube3[3][4]
    cube3[3][3]=cube3[3][5]
    cube3[3][4]=cube3[4][5]
    cube3[3][5]=cube3[5][5]
    cube3[4][5]=cube3[5][4]
    cube3[5][5]=cube3[5][3]
    cube3[5][4]=cube3[4][3]
    cube3[5][3]=aa
    cube3[4][3]=bb
    afficher(cube3)
    
def D():
    #Définie le mouvement Down (Bas)
    xx=cube3[5][6]
    yy=cube3[5][7]
    zz=cube3[5][8]
    cube3[5][6]=cube3[5][3]
    cube3[5][7]=cube3[5][4]
    cube3[5][8]=cube3[5][5]
    cube3[5][3]=cube3[5][0]
    cube3[5][4]=cube3[5][1]
    cube3[5][5]=cube3[5][2]
    cube3[5][0]=cube3[5][9]
    cube3[5][1]=cube3[5][10]
    cube3[5][2]=cube3[5][11]
    cube3[5][9]=xx
    cube3[5][10]=yy
    cube3[5][11]=zz
    aa=cube3[6][6]
    bb=cube3[6][7]
    cube3[6][6]=cube3[8][6]
    cube3[6][7]=cube3[7][6]
    cube3[8][6]=cube3[8][8]
    cube3[7][6]=cube3[8][7]
    cube3[8][8]=cube3[6][8]
    cube3[8][7]=cube3[7][8]
    cube3[6][8]=aa
    cube3[7][8]=bb 
    afficher(cube3)

def D_prime():
    #Définie le mouvement Down (Bas) inversé
    xx=cube3[5][6]
    yy=cube3[5][7]
    zz=cube3[5][8]
    cube3[5][6]=cube3[5][9]
    cube3[5][7]=cube3[5][10]
    cube3[5][8]=cube3[5][11]
    cube3[5][9]=cube3[5][2]
    cube3[5][10]=cube3[5][1]
    cube3[5][11]=cube3[5][0]
    cube3[5][2]=cube3[5][5]
    cube3[5][1]=cube3[5][4]
    cube3[5][0]=cube3[5][3]
    cube3[5][5]=zz
    cube3[5][4]=yy
    cube3[5][3]=xx
    aa=cube3[6][6]
    bb=cube3[6][7]
    cube3[6][6]=cube3[6][8]
    cube3[6][7]=cube3[7][8]
    cube3[6][8]=cube3[8][8]
    cube3[7][8]=cube3[8][7]
    cube3[8][8]=cube3[8][6]
    cube3[8][7]=cube3[7][6]
    cube3[8][6]=aa
    cube3[7][6]=bb
    afficher(cube3)
    

button1=Button(fenetre,text="Fi mouvement", width=15,command=F_prime ,bg="grey80")
button1.place(x=620,y=250)
button1=Button(fenetre,text="F mouvement", width=15,command=F ,bg="grey80")
button1.place(x=500,y=250)
button1=Button(fenetre,text="R mouvement", width=15,command=R ,bg="grey80")
button1.place(x=500,y=280)
button1=Button(fenetre,text="Ri mouvement", width=15,command=R_prime ,bg="grey80")
button1.place(x=620,y=280)
button1=Button(fenetre,text="B mouvement", width=15,command=B ,bg="grey80")
button1.place(x=500,y=310)
button1=Button(fenetre,text="Bi mouvement", width=15,command=B_prime ,bg="grey80")
button1.place(x=620,y=310)
button1=Button(fenetre,text="U mouvement", width=15,command=U ,bg="grey80")
button1.place(x=500,y=340)
button1=Button(fenetre,text="Ui mouvement", width=15,command=U_prime ,bg="grey80")
button1.place(x=620,y=340)
button1=Button(fenetre,text="L mouvement", width=15,command=L ,bg="grey80")
button1.place(x=500,y=370)
button1=Button(fenetre,text="Li mouvement", width=15,command=L_prime ,bg="grey80")
button1.place(x=620,y=370)
button1=Button(fenetre,text="D mouvement", width=15,command=D ,bg="grey80")
button1.place(x=500,y=400)
button1=Button(fenetre,text="Di mouvement", width=15,command=D_prime ,bg="grey80")
button1.place(x=620,y=400)

button1=Button(fenetre,text="croix", width=15,command=solve_croix ,bg="grey80")
button1.place(x=100,y=50)



button1=Button(fenetre,text="bleu", width=8,command=bleu ,bg="blue")
button1.place(x=50,y=400)
button1=Button(fenetre,text="jaune", width=8,command=jaune ,bg="yellow")
button1.place(x=120,y=400)
button1=Button(fenetre,text="rouge", width=8,command=rouge ,bg="red")
button1.place(x=190,y=400)
button1=Button(fenetre,text="blanc", width=8,command=blanc ,bg="white")
button1.place(x=260,y=400)
button1=Button(fenetre,text="orange", width=8,command=orange ,bg="orange")
button1.place(x=330,y=400)
button1=Button(fenetre,text="vert", width=8,command=vert ,bg="green")
button1.place(x=400,y=400)

button=Button(fenetre,text="Sortir", width=8,command=fenetre.destroy ,fg='red',bg="grey90")
button.place(x=410,y=10)

button1=Button(fenetre,text="Réinitialiser", width=10,height=2,command=reinit ,bg="cyan")
button1.place(x=10,y=60)
button1=Button(fenetre,text="Résoudre", width=10,height=2,command=resoudre ,bg="grey80")
button1.place(x=10,y=10)


def clique(event):
    x,y=event.x,event.y
    num_line=0
    num_case=0
    global couleur
    global cube3
    global cub
    global fenetre
    global canvas
    global taille
    if x>=0 and y>=0 and x<=taille*largeur and y<=taille*hauteur:
        for line in cube3:
            num_line=num_line+1
            for case in line:
                num_case=num_case+1
                if cube3[1][7]!=1 or cube3[4][1]!=2 or cube3[4][4] !=5 or cube3[4][7] !=4 or cube3[4][10] !=3 or cube3[7][7] !=6:
                    cube3[1][7]=1
                    cube3[4][1]=2
                    cube3[4][4]=5
                    cube3[4][7]=4
                    cube3[4][10]=3
                    cube3[7][7]=6
                    afficher(cube3)
                elif case!=0 and x>=(num_case-1)*taille and y>=(num_line-1)*taille and x<=(num_case)*taille  and  y<=(num_line)*taille:
                    cube3[num_line-1][num_case-1]=couleur
                    print(cube3)
                    afficher(cube3)



            num_case=0

    canvas.delete('all')
    afficher(cube3)

afficher(cube3)